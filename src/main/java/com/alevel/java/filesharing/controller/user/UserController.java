package com.alevel.java.filesharing.controller.user;

import com.alevel.java.filesharing.Routes;
import com.alevel.java.filesharing.exceptions.FileSharingExceptions;
import com.alevel.java.filesharing.model.file.response.FileMetadataResponse;
import com.alevel.java.filesharing.model.user.request.*;
import com.alevel.java.filesharing.model.user.response.UserResponse;
import com.alevel.java.filesharing.service.file.FileMetadataOperations;
import com.alevel.java.filesharing.service.user.UserOperations;
import io.swagger.v3.oas.annotations.Parameter;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(Routes.USERS)
public class UserController {

    private final UserOperations userOperations;

    private final FileMetadataOperations filesForUserOperations;

    public UserController(UserOperations userOperations, FileMetadataOperations filesForUserOperations) {
        this.userOperations = userOperations;
        this.filesForUserOperations = filesForUserOperations;
    }

    //region user registration

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserResponse register(@RequestBody @Valid SaveUserRequest request) {
        return userOperations.create(request);
    }

    //endregion

    //region authenticated user API

    @GetMapping("/me")
    public UserResponse getCurrentUser(@AuthenticationPrincipal String email) {
        return userOperations.findByEmail(email).orElseThrow(() -> FileSharingExceptions.userNotFound(email));
    }

    @PatchMapping("/me")
    public UserResponse mergeCurrentUser(@AuthenticationPrincipal String email,
                                         @RequestBody @Valid MergeUserRequest request) {
        return userOperations.mergeByEmail(email, request);
    }

    @PatchMapping("/me/password")
    public UserResponse changeCurrentUserPassword(@AuthenticationPrincipal String email,
                                                  @RequestBody @Valid ChangeUserPasswordRequest request) {
        return userOperations.changePasswordByEmail(email, request);
    }

    @DeleteMapping("/me")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCurrentUser(@AuthenticationPrincipal String email) {
        userOperations.deleteByEmail(email);
    }

    @GetMapping("/{id}")
    public UserResponse getUserById(@PathVariable long id) {
        return userOperations.findById(id).orElseThrow(() -> FileSharingExceptions.userNotFound(id));
    }

    @GetMapping
    @PageableAsQueryParam
    public Page<UserResponse> listUsers(@Parameter(hidden = true) Pageable pageable) {
        return userOperations.list(pageable);
    }

    //endregion

    //region admin-only API

    @PostMapping("/admins")
    @ResponseStatus(HttpStatus.CREATED)
    public UserResponse registerAdmin(@RequestBody @Valid SaveUserRequest request) {
        return userOperations.createAdmin(request);
    }

    @GetMapping("/{id}/files")
    @PageableAsQueryParam
    public Page<FileMetadataResponse> getFilesById(
            @PathVariable long id,
            @RequestParam(name = "include-shared", defaultValue = "false") boolean includeShared,
            @Parameter(hidden = true) Pageable pageable
    ) {
        return includeShared
                ? filesForUserOperations.getAvailableFiles(id, pageable)
                : filesForUserOperations.getOwnFiles(id, pageable);
    }

    @PatchMapping("/{id}")
    public UserResponse mergeUserById(@PathVariable long id,
                                      @RequestBody @Valid MergeUserRequest request) {
        return userOperations.mergeById(id, request);
    }

    @PatchMapping("/{id}/status")
    public UserResponse changeUserStatusById(@PathVariable long id,
                                             @RequestBody @Valid ChangeUserStatusRequest request) {
        return userOperations.changeStatusById(id, request.status());
    }

    @PatchMapping("/{id}/password")
    public UserResponse changeUserPassword(@PathVariable long id,
                                           @RequestBody @Valid OverrideUserPasswordRequest request) {
        return userOperations.changePasswordById(id, request);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUserById(@PathVariable long id) {
        userOperations.deleteById(id);
    }

    //endregion

}
