package com.alevel.java.filesharing.controller.file;

import com.alevel.java.filesharing.Routes;
import com.alevel.java.filesharing.exceptions.FileSharingExceptions;
import com.alevel.java.filesharing.model.file.response.FileMetadataResponse;
import com.alevel.java.filesharing.model.file.request.UpdateFileSharingOptionsRequest;
import com.alevel.java.filesharing.service.file.FileOperations;
import io.swagger.v3.oas.annotations.Parameter;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(Routes.FILES)
public class FileController {

    private final FileOperations fileOperations;

    public FileController(FileOperations fileOperations) {
        this.fileOperations = fileOperations;
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public FileMetadataResponse upload(@RequestPart MultipartFile file, Authentication authentication) {
        return fileOperations.store(file, authentication);
    }

    @GetMapping
    @PageableAsQueryParam
    public Page<FileMetadataResponse> getAvailableFiles(
            Authentication authentication,
            @RequestParam(name ="include-shared", defaultValue = "false") boolean includeShared,
            @Parameter(hidden = true) Pageable pageable
    ) {
        return includeShared
                ? fileOperations.getAvailableFiles(authentication, pageable)
                : fileOperations.getOwnFiles(authentication, pageable);
    }

    @GetMapping("/{id}/metadata")
    public FileMetadataResponse getMetadata(@PathVariable UUID id, Authentication authentication) {
        return fileOperations.getMetadata(id, authentication)
                .orElseThrow(() -> FileSharingExceptions.fileNotFound(id));
    }

    @GetMapping(value = "/{id}", produces = MediaType.ALL_VALUE)
    public ResponseEntity<Resource> download(@PathVariable UUID id, Authentication authentication) {
        var file = fileOperations.loadAsResource(id, authentication)
                .orElseThrow(() -> FileSharingExceptions.fileNotFound(id));

        var metadata = file.getMetadata();

        var contentDisposition = ContentDisposition.attachment()
                .filename(metadata.name())
                .build();

        var contentType = Optional.ofNullable(metadata.contentType())
                .map(MediaType::parseMediaType)
                .orElse(MediaType.APPLICATION_OCTET_STREAM);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString())
                .contentType(contentType)
                .body(file.getResource());
    }

    @PutMapping(
            value = "/{id}",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public FileMetadataResponse replace(
            @PathVariable UUID id,
            @RequestPart MultipartFile file,
            Authentication authentication
    ) {
        return fileOperations.replace(id, file, authentication);
    }

    @PatchMapping("/{id}")
    public FileMetadataResponse updateSharingOptions(
            @PathVariable UUID id,
            @RequestBody @Valid UpdateFileSharingOptionsRequest request,
            Authentication authentication
    ) {
        return fileOperations.share(id, request, authentication);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable UUID id, Authentication authentication) {
        fileOperations.deleteById(id, authentication);
    }

}
