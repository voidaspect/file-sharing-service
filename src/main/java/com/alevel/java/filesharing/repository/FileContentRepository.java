package com.alevel.java.filesharing.repository;

import com.alevel.java.filesharing.model.file.FileContent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface FileContentRepository extends JpaRepository<FileContent, UUID> {
}
