package com.alevel.java.filesharing.repository;

import com.alevel.java.filesharing.model.user.FileSharingUser;
import com.alevel.java.filesharing.model.user.UserStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserRepository extends JpaRepository<FileSharingUser, Long> {

    Optional<FileSharingUser> findByEmail(String email);

    Optional<FileSharingUser> findByEmailOrNickname(String email, String nickname);

    boolean existsByEmail(String email);

    boolean existsByNickname(String nickname);

    void deleteByEmail(String email);

    @Query("update FileSharingUser u set u.status = :status where u.email = :email")
    @Modifying
    void changeStatusByEmail(String email, UserStatus status);

}
