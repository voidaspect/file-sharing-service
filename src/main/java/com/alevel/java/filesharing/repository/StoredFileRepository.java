package com.alevel.java.filesharing.repository;

import com.alevel.java.filesharing.model.file.StoredFile;
import com.alevel.java.filesharing.model.user.FileSharingUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface StoredFileRepository extends JpaRepository<StoredFile, UUID> {

    @Query("select sf from StoredFile sf where sf.owner = :user or :user member of sf.sharedWith")
    Page<StoredFile> findOwnedAndSharedFiles(FileSharingUser user, Pageable pageable);

    @Query("select sf from StoredFile sf where sf.owner = :user")
    Page<StoredFile> findOwnedFiles(FileSharingUser user, Pageable pageable);

    @Query("select case when count(u.id) > 0 then true else false end " +
            "from FileSharingUser u where u.email = :email and :file member of u.filesSharedWith")
    boolean isSharedWithByEmail(String email, StoredFile file);

}
