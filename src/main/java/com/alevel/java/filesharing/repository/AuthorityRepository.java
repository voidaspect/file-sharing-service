package com.alevel.java.filesharing.repository;

import com.alevel.java.filesharing.model.user.FileSharingUserAuthority;
import com.alevel.java.filesharing.model.user.KnownAuthority;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.EnumSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

public interface AuthorityRepository extends JpaRepository<FileSharingUserAuthority, KnownAuthority> {

    Set<KnownAuthority> ADMIN_AUTHORITIES = EnumSet.of(KnownAuthority.ROLE_USER, KnownAuthority.ROLE_ADMIN);

    Stream<FileSharingUserAuthority> findAllByIdIn(Set<KnownAuthority> ids);

}
