package com.alevel.java.filesharing.model.user.request;

import com.alevel.java.filesharing.model.user.UserStatus;

import javax.validation.constraints.NotNull;

public record ChangeUserStatusRequest(@NotNull UserStatus status) {
}
