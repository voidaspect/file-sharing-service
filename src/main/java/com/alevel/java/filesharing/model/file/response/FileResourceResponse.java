package com.alevel.java.filesharing.model.file.response;

import org.springframework.core.io.Resource;

public record FileResourceResponse(Resource resource,
                                   FileMetadataResponse metadata) {

    public Resource getResource() {
        return resource;
    }

    public FileMetadataResponse getMetadata() {
        return metadata;
    }

}
