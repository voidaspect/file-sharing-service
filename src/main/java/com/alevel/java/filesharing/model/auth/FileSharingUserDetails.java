package com.alevel.java.filesharing.model.auth;

import com.alevel.java.filesharing.model.user.FileSharingUser;
import com.alevel.java.filesharing.model.user.UserStatus;
import org.springframework.security.core.userdetails.User;

import java.util.EnumSet;

public class FileSharingUserDetails extends User {

    private final FileSharingUser source;

    public FileSharingUserDetails(FileSharingUser source) {
        super(source.getEmail(),
                source.getPassword(),
                source.getStatus() == UserStatus.ACTIVE,
                true,
                true,
                true,
                EnumSet.copyOf(source.getAuthorities().keySet())
        );
        this.source = source;
    }

    public FileSharingUser getSource() {
        return source;
    }
}
