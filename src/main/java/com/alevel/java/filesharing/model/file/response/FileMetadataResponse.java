package com.alevel.java.filesharing.model.file.response;

import com.alevel.java.filesharing.Routes;
import com.alevel.java.filesharing.model.ResourceResponse;
import com.alevel.java.filesharing.model.file.StoredFile;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

public record FileMetadataResponse(UUID id,
                                   String name,
                                   UserRepresentation owner,
                                   @JsonFormat(shape = JsonFormat.Shape.STRING)
                                   OffsetDateTime uploadedAt,
                                   int size,
                                   String contentType,
                                   @JsonInclude(JsonInclude.Include.NON_NULL)
                                   Set<UserRepresentation> sharedWith) implements ResourceResponse {

    public static FileMetadataResponse fromStoredFile(StoredFile source) {
        var sharedWith = source.getSharedWith().keySet().stream()
                .map(UserRepresentation::new)
                .collect(Collectors.toSet());
        return new FileMetadataResponse(
                source.getId(),
                source.getName(),
                new UserRepresentation(source.getOwner().getId()),
                source.getUploadedAt(),
                source.getSize(),
                source.getContentType(),
                sharedWith);
    }

    // only the attributes that don't require extra fetching
    public static FileMetadataResponse fromStoredFileWithBasicAttributes(StoredFile source) {
        return new FileMetadataResponse(
                source.getId(),
                source.getName(),
                new UserRepresentation(source.getOwner().getId()),
                source.getUploadedAt(),
                source.getSize(),
                source.getContentType(),
                null);
    }

    public record UserRepresentation(long id) implements ResourceResponse {

        @Override
        @JsonProperty(access = JsonProperty.Access.READ_ONLY)
        public String path() {
            return Routes.user(id);
        }

    }

    @Override
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String path() {
        return Routes.file(id);
    }

}
