package com.alevel.java.filesharing.model.auth.response;

public record AccessTokenResponse(String accessToken, String refreshToken, long expireIn) {

}
