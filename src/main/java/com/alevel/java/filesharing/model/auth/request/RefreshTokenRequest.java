package com.alevel.java.filesharing.model.auth.request;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public record RefreshTokenRequest(@NotNull String refreshToken) {
}
