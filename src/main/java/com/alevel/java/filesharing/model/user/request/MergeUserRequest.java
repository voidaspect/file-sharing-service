package com.alevel.java.filesharing.model.user.request;

import com.alevel.java.filesharing.model.constraints.NullableNotBlank;

import javax.validation.constraints.Email;

public record MergeUserRequest(
        @Email(message = "email must be a valid email string")
        String email,
        @NullableNotBlank(message = "nickname must not be blank")
        String nickname
) {
}
