package com.alevel.java.filesharing.model.file.request;

import java.util.Set;

public record UpdateFileSharingOptionsRequest(Set<Long> added,
                                              Set<Long> removed) {
}
