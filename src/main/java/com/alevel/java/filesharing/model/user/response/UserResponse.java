package com.alevel.java.filesharing.model.user.response;

import com.alevel.java.filesharing.Routes;
import com.alevel.java.filesharing.model.ResourceResponse;
import com.alevel.java.filesharing.model.user.FileSharingUser;
import com.alevel.java.filesharing.model.user.KnownAuthority;
import com.alevel.java.filesharing.model.user.UserStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.OffsetDateTime;
import java.util.EnumSet;
import java.util.Set;

public record UserResponse(long id,
                           String email,
                           String nickname,
                           UserStatus status,
                           @JsonFormat(shape = JsonFormat.Shape.STRING)
                           OffsetDateTime createdAt,
                           @JsonInclude(JsonInclude.Include.NON_NULL)
                           Set<KnownAuthority> authorities) implements ResourceResponse {

    public static UserResponse fromUser(FileSharingUser user) {
        return new UserResponse(
                user.getId(),
                user.getEmail(),
                user.getNickname(),
                user.getStatus(),
                user.getCreatedAt(),
                EnumSet.copyOf(user.getAuthorities().keySet()));
    }

    // only the attributes that don't require extra fetching
    public static UserResponse fromUserWithBasicAttributes(FileSharingUser user) {
        return new UserResponse(
                user.getId(),
                user.getEmail(),
                user.getNickname(),
                user.getStatus(),
                user.getCreatedAt(),
                null);
    }

    @Override
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String path() {
        return Routes.user(id);
    }

}
