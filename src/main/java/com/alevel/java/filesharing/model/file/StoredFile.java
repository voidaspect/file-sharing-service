package com.alevel.java.filesharing.model.file;

import com.alevel.java.filesharing.model.user.FileSharingUser;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "files")
public class StoredFile {

    public static final String DEFAULT_NAME = "unnamed";

    @Id
    private UUID id;

    @Column(nullable = false)
    private String name;

    @Column(name = "uploaded_at", nullable = false)
    private OffsetDateTime uploadedAt;

    @Access(AccessType.PROPERTY)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", nullable = false)
    private FileSharingUser owner;

    @ManyToMany
    @JoinTable(
            name = "shared_files",
            joinColumns = @JoinColumn(name = "file_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id")
    )
    @MapKey
    @SuppressWarnings("FieldMayBeFinal")
    private Map<Long, FileSharingUser> sharedWith = new HashMap<>();

    @Column(nullable = false)
    private int size;

    @Column(name = "content_type")
    private String contentType;

    @Version
    private Integer version;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OffsetDateTime getUploadedAt() {
        return uploadedAt;
    }

    public void setUploadedAt(OffsetDateTime uploadedAt) {
        this.uploadedAt = uploadedAt;
    }

    public FileSharingUser getOwner() {
        return owner;
    }

    public void setOwner(FileSharingUser owner) {
        this.owner = owner;
    }

    public Map<Long, FileSharingUser> getSharedWith() {
        return sharedWith;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StoredFile metadata = (StoredFile) o;
        return Objects.equals(id, metadata.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

}
