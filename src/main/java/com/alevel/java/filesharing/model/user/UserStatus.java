package com.alevel.java.filesharing.model.user;

public enum UserStatus {
    ACTIVE, SUSPENDED
}
