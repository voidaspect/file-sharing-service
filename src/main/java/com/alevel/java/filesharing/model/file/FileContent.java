package com.alevel.java.filesharing.model.file;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "file_content")
public class FileContent {

    @Id
    @Column(name = "file_id", nullable = false)
    private UUID fileId;

    @MapsId
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "file_id", nullable = false)
    private StoredFile metadata;

    @Column(nullable = false)
    private byte[] content;

    public UUID getFileId() {
        return fileId;
    }

    public void setFileId(UUID fileId) {
        this.fileId = fileId;
    }

    public StoredFile getMetadata() {
        return metadata;
    }

    public void setMetadata(StoredFile metadata) {
        this.metadata = metadata;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }
}
