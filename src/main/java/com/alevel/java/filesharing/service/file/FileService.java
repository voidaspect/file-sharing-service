package com.alevel.java.filesharing.service.file;

import com.alevel.java.filesharing.exceptions.FileSharingExceptions;
import com.alevel.java.filesharing.model.file.FileContent;
import com.alevel.java.filesharing.model.file.response.FileMetadataResponse;
import com.alevel.java.filesharing.model.file.response.FileResourceResponse;
import com.alevel.java.filesharing.model.file.StoredFile;
import com.alevel.java.filesharing.model.file.request.UpdateFileSharingOptionsRequest;
import com.alevel.java.filesharing.model.user.FileSharingUser;
import com.alevel.java.filesharing.model.user.KnownAuthority;
import com.alevel.java.filesharing.model.user.UserStatus;
import com.alevel.java.filesharing.repository.FileContentRepository;
import com.alevel.java.filesharing.repository.StoredFileRepository;
import com.alevel.java.filesharing.repository.UserRepository;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class FileService implements FileOperations {

    private static final Logger log = LoggerFactory.getLogger(FileService.class);

    private final UserRepository userRepository;

    private final StoredFileRepository storedFileRepository;

    private final FileContentRepository fileContentRepository;

    private final Tika tika;

    public FileService(UserRepository userRepository,
                       StoredFileRepository storedFileRepository,
                       FileContentRepository fileContentRepository,
                       Tika tika) {
        this.userRepository = userRepository;
        this.storedFileRepository = storedFileRepository;
        this.fileContentRepository = fileContentRepository;
        this.tika = tika;
    }

    @Override
    @Transactional
    public FileMetadataResponse store(MultipartFile file, Authentication authentication) {
        FileSharingUser user = getUser(authentication);

        var stored = new StoredFile();
        stored.setOwner(user);
        var content = updateContent(file, stored);
        stored.setId(UUID.randomUUID());
        storedFileRepository.save(stored);
        fileContentRepository.save(content);

        return FileMetadataResponse.fromStoredFile(stored);
    }

    @Override
    @Transactional
    public FileMetadataResponse replace(UUID fileId, MultipartFile file, Authentication authentication) {
        StoredFile stored = getFile(fileId);

        checkOwnershipRights(authentication, stored);

        updateContent(file, stored);

        return FileMetadataResponse.fromStoredFile(stored);
    }

    @Override
    @Transactional
    public FileMetadataResponse share(UUID fileId, UpdateFileSharingOptionsRequest request, Authentication authentication) {
        StoredFile file = getFile(fileId);

        checkOwnershipRights(authentication, file);

        Set<Long> added = Objects.requireNonNullElseGet(request.added(), Collections::emptySet);
        Set<Long> removed = Objects.requireNonNullElseGet(request.removed(), Collections::emptySet);

        // normalize (when added and removed cancel each other)
        if (added.size() < removed.size()) {
            added.removeIf(removed::remove);
        } else {
            removed.removeIf(added::remove);
        }

        // can't change access for owner
        Long ownerId = file.getOwner().getId();
        added.remove(ownerId);
        removed.remove(ownerId);

        Map<Long, FileSharingUser> sharedWith = file.getSharedWith();
        Set<Long> sharedWithIds = sharedWith.keySet();

        // remove users by id
        sharedWithIds.removeAll(removed);

        // can't add if already added
        added.removeAll(sharedWithIds);

        List<FileSharingUser> addedUsers = userRepository.findAllById(added);

        for (var addedUser : addedUsers) {
            if (addedUser.getStatus() != UserStatus.ACTIVE) {
                log.warn("User {} shared file {} with non-active user {}",
                        authentication.getPrincipal(), file.getId(), addedUser.getEmail());
            }
            sharedWith.put(addedUser.getId(), addedUser);
        }

        return FileMetadataResponse.fromStoredFile(file);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<FileResourceResponse> loadAsResource(UUID fileId, Authentication authentication) {
        return storedFileRepository.findById(fileId).map(file -> {
            checkAccessRights(authentication, file);
            byte[] content = fileContentRepository.getById(fileId).getContent();
            var resource = new ByteArrayResource(content);
            return new FileResourceResponse(resource, FileMetadataResponse.fromStoredFile(file));
        });
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<FileMetadataResponse> getMetadata(UUID fileId, Authentication authentication) {
        Optional<StoredFile> metadata = storedFileRepository.findById(fileId);

        metadata.ifPresent(file -> checkAccessRights(authentication, file));

        return metadata.map(FileMetadataResponse::fromStoredFile);
    }

    @Override
    @Transactional
    public void deleteById(UUID id, Authentication authentication) {
        StoredFile file = getFile(id);

        checkOwnershipRights(authentication, file);

        storedFileRepository.delete(file);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<FileMetadataResponse> getOwnFiles(long userId, Pageable pageable) {
        FileSharingUser user = getUser(userId);
        return storedFileRepository.findOwnedFiles(user, pageable)
                .map(FileMetadataResponse::fromStoredFileWithBasicAttributes);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<FileMetadataResponse> getAvailableFiles(long userId, Pageable pageable) {
        FileSharingUser user = getUser(userId);
        return storedFileRepository.findOwnedAndSharedFiles(user, pageable)
                .map(FileMetadataResponse::fromStoredFileWithBasicAttributes);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<FileMetadataResponse> getOwnFiles(Authentication authentication, Pageable pageable) {
        FileSharingUser user = getUser(authentication);
        return storedFileRepository.findOwnedFiles(user, pageable)
                .map(FileMetadataResponse::fromStoredFileWithBasicAttributes);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<FileMetadataResponse> getAvailableFiles(Authentication authentication, Pageable pageable) {
        FileSharingUser user = getUser(authentication);
        return storedFileRepository.findOwnedAndSharedFiles(user, pageable)
                .map(FileMetadataResponse::fromStoredFileWithBasicAttributes);
    }

    private void checkAccessRights(Authentication authentication, StoredFile file) {
        var email = ((String) authentication.getPrincipal());
        boolean hasRights = isAdmin(authentication) || isOwner(email, file) || hasBeenSharedWith(email, file);
        if (hasRights) return;
        throw FileSharingExceptions.noAccessRightsToFile(email, file.getId());
    }

    private void checkOwnershipRights(Authentication authentication, StoredFile file) {
        var email = (String) authentication.getPrincipal();
        boolean hasRights = isAdmin(authentication) || isOwner(email, file);
        if (hasRights) return;
        throw FileSharingExceptions.noEditingRightsToFile(email, file.getId());
    }

    private boolean isAdmin(Authentication authentication) {
        return authentication.getAuthorities().contains(KnownAuthority.ROLE_ADMIN);
    }

    private boolean isOwner(String email, StoredFile file) {
        return file.getOwner().getEmail().equals(email);
    }

    private boolean hasBeenSharedWith(String email, StoredFile file) {
        return storedFileRepository.isSharedWithByEmail(email, file);
    }

    private StoredFile getFile(UUID id) {
        return storedFileRepository.findById(id)
                .orElseThrow(() -> FileSharingExceptions.fileNotFound(id));
    }

    private FileSharingUser getUser(Authentication authentication) {
        var email = (String) authentication.getPrincipal();
        return userRepository.findByEmail(email)
                .orElseThrow(() -> FileSharingExceptions.userNotFound(email));
    }

    private FileSharingUser getUser(long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> FileSharingExceptions.userNotFound(id));
    }

    private FileContent updateContent(MultipartFile file, StoredFile stored) {
        if (file.isEmpty()) throw FileSharingExceptions.emptyFile();
        byte[] content;
        try {
            content = file.getBytes();
        } catch (IOException e) {
            log.error("Failed to read file content", e);
            throw FileSharingExceptions.storageOperationFailed(e);
        }
        FileContent fileContent;
        if (stored.getId() == null) {
            fileContent = new FileContent();
            fileContent.setMetadata(stored);
        } else {
            fileContent = fileContentRepository.getById(stored.getId());
        }
        fileContent.setContent(content);
        String name = Objects.requireNonNullElse(file.getOriginalFilename(), StoredFile.DEFAULT_NAME);
        stored.setName(name);
        stored.setSize(content.length);
        stored.setUploadedAt(OffsetDateTime.now());
        // extract and validate content-type
        String detectedContentType = tika.detect(content);
        String providedContentType = file.getContentType();
        if (providedContentType != null && !providedContentType.equals(detectedContentType)) {
            log.warn(
                    "Provided content type of file {} ({}) doesn't match its apparent content type, which is {}",
                    name, providedContentType, detectedContentType
            );
        }
        // use detected content-type to ensure data integrity
        stored.setContentType(detectedContentType);
        return fileContent;
    }
}
