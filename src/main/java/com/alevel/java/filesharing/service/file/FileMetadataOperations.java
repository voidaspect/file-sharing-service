package com.alevel.java.filesharing.service.file;

import com.alevel.java.filesharing.model.file.response.FileMetadataResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;

import java.util.Optional;
import java.util.UUID;

public interface FileMetadataOperations {

    Page<FileMetadataResponse> getOwnFiles(long userId, Pageable pageable);

    Page<FileMetadataResponse> getAvailableFiles(long userId, Pageable pageable);

    Page<FileMetadataResponse> getOwnFiles(Authentication authentication, Pageable pageable);

    Page<FileMetadataResponse> getAvailableFiles(Authentication authentication, Pageable pageable);

    Optional<FileMetadataResponse> getMetadata(UUID fileId, Authentication authentication);

}
