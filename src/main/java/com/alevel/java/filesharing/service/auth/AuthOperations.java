package com.alevel.java.filesharing.service.auth;

import com.alevel.java.filesharing.exceptions.auth.InvalidRefreshTokenException;
import com.alevel.java.filesharing.model.auth.FileSharingUserDetails;
import com.alevel.java.filesharing.model.auth.response.AccessTokenResponse;

public interface AuthOperations {

    AccessTokenResponse getToken(FileSharingUserDetails userDetails);

    AccessTokenResponse refreshToken(String refreshToken)
            throws InvalidRefreshTokenException;

    void invalidateToken(String refreshToken, String ownerEmail) throws InvalidRefreshTokenException;

}
