package com.alevel.java.filesharing.service.user;

import com.alevel.java.filesharing.model.user.UserStatus;
import com.alevel.java.filesharing.model.user.request.ChangeUserPasswordRequest;
import com.alevel.java.filesharing.model.user.request.MergeUserRequest;
import com.alevel.java.filesharing.model.user.request.OverrideUserPasswordRequest;
import com.alevel.java.filesharing.model.user.request.SaveUserRequest;
import com.alevel.java.filesharing.model.user.response.UserResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface UserOperations {

    Page<UserResponse> list(Pageable pageable);

    Optional<UserResponse> findById(long id);

    Optional<UserResponse> findByEmail(String email);

    UserResponse mergeById(long id, MergeUserRequest request);

    UserResponse mergeByEmail(String email, MergeUserRequest request);

    UserResponse create(SaveUserRequest request);

    UserResponse createAdmin(SaveUserRequest request);

    UserResponse changeStatusById(long id, UserStatus status);

    UserResponse changePasswordById(long id, OverrideUserPasswordRequest request);

    UserResponse changePasswordByEmail(String email, ChangeUserPasswordRequest request);

    void deleteById(long id);

    void deleteByEmail(String email);
}
