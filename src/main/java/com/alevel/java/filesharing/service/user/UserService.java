package com.alevel.java.filesharing.service.user;

import com.alevel.java.filesharing.exceptions.FileSharingExceptions;
import com.alevel.java.filesharing.model.auth.FileSharingUserDetails;
import com.alevel.java.filesharing.model.user.FileSharingUser;
import com.alevel.java.filesharing.model.user.FileSharingUserAuthority;
import com.alevel.java.filesharing.model.user.KnownAuthority;
import com.alevel.java.filesharing.model.user.UserStatus;
import com.alevel.java.filesharing.model.user.request.ChangeUserPasswordRequest;
import com.alevel.java.filesharing.model.user.request.MergeUserRequest;
import com.alevel.java.filesharing.model.user.request.OverrideUserPasswordRequest;
import com.alevel.java.filesharing.model.user.request.SaveUserRequest;
import com.alevel.java.filesharing.model.user.response.UserResponse;
import com.alevel.java.filesharing.repository.AuthorityRepository;
import com.alevel.java.filesharing.repository.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService, UserOperations {

    private final UserRepository userRepository;

    private final AuthorityRepository authorityRepository;

    private final PasswordEncoder passwordEncoder;

    public UserService(
            UserRepository userRepository,
            AuthorityRepository authorityRepository,
            PasswordEncoder passwordEncoder
    ) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        FileSharingUser user = userRepository.findByEmailOrNickname(username, username)
                .orElseThrow(() -> new UsernameNotFoundException("User " + username + " not found"));

        return new FileSharingUserDetails(user);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserResponse> list(Pageable pageable) {
        return userRepository.findAll(pageable).map(UserResponse::fromUserWithBasicAttributes);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserResponse> findById(long id) {
        return userRepository.findById(id).map(UserResponse::fromUser);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<UserResponse> findByEmail(String email) {
        return userRepository.findByEmail(email).map(UserResponse::fromUser);
    }

    @Override
    @Transactional
    public UserResponse mergeById(long id, MergeUserRequest request) {
        FileSharingUser user = getUser(id);
        return UserResponse.fromUser(merge(user, request));
    }

    @Override
    @Transactional
    public UserResponse mergeByEmail(String email, MergeUserRequest request) {
        FileSharingUser user = getUser(email);
        return UserResponse.fromUser(merge(user, request));
    }

    @Override
    @Transactional
    public UserResponse create(SaveUserRequest request) {
        validateUniqueFields(request);
        return UserResponse.fromUser(save(request, getRegularUserAuthorities()));
    }

    @Override
    @Transactional
    public UserResponse createAdmin(SaveUserRequest request) {
        validateUniqueFields(request);
        return UserResponse.fromUser(save(request, getAdminAuthorities()));
    }

    @Override
    @Transactional
    public UserResponse changeStatusById(long id, UserStatus status) {
        FileSharingUser user = getUser(id);
        if (user.getStatus() != status) {
            user.setStatus(status);
        }
        return UserResponse.fromUser(user);
    }

    @Override
    @Transactional
    public UserResponse changePasswordById(long id, OverrideUserPasswordRequest request) {
        FileSharingUser user = getUser(id);
        user.setPassword(passwordEncoder.encode(request.password()));
        return UserResponse.fromUser(user);
    }

    @Override
    @Transactional
    public UserResponse changePasswordByEmail(String email, ChangeUserPasswordRequest request) {
        FileSharingUser user = getUser(email);
        changePassword(user, request.oldPassword(), request.newPassword());
        return UserResponse.fromUser(user);
    }

    @Override
    @Transactional
    public void deleteById(long id) {
        if (!userRepository.existsById(id)) throw FileSharingExceptions.userNotFound(id);
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void deleteByEmail(String email) {
        if (!userRepository.existsByEmail(email)) throw FileSharingExceptions.userNotFound(email);
        userRepository.deleteByEmail(email);
    }

    @Transactional
    public void mergeAdmins(List<SaveUserRequest> requests) {
        if (requests.isEmpty()) return;
        Map<KnownAuthority, FileSharingUserAuthority> authorities = getAdminAuthorities();
        for (SaveUserRequest request : requests) {
            String email = request.email();
            String nickname = request.nickname();
            FileSharingUser user = userRepository.findByEmail(email).orElseGet(() -> {
                var newUser = new FileSharingUser();
                newUser.setCreatedAt(OffsetDateTime.now());
                newUser.setEmail(email);
                return newUser;
            });
            if (!nickname.equals(user.getNickname())) {
                if (userRepository.existsByNickname(nickname)) throw FileSharingExceptions.duplicateNickname(nickname);
                user.setNickname(nickname);
            }
            user.setPassword(passwordEncoder.encode(request.password()));
            user.getAuthorities().putAll(authorities);
            userRepository.save(user);
        }
    }

    private FileSharingUser getUser(long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> FileSharingExceptions.userNotFound(id));
    }

    private FileSharingUser getUser(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> FileSharingExceptions.userNotFound(email));
    }

    private FileSharingUser merge(FileSharingUser user, MergeUserRequest request) {
        String email = request.email();
        if (email != null && !email.equals(user.getEmail())) {
            if (userRepository.existsByEmail(email)) throw FileSharingExceptions.duplicateEmail(email);
            user.setEmail(email);
        }
        String nickname = request.nickname();
        if (nickname != null && !nickname.equals(user.getNickname())) {
            if (userRepository.existsByNickname(nickname)) throw FileSharingExceptions.duplicateNickname(nickname);
            user.setNickname(nickname);
        }
        return user;
    }

    private FileSharingUser save(SaveUserRequest request, Map<KnownAuthority, FileSharingUserAuthority> authorities) {
        var user = new FileSharingUser();
        user.getAuthorities().putAll(authorities);
        user.setEmail(request.email());
        user.setNickname(request.nickname());
        user.setPassword(passwordEncoder.encode(request.password()));
        user.setCreatedAt(OffsetDateTime.now());
        userRepository.save(user);
        return user;
    }

    private void changePassword(FileSharingUser user, String oldPassword, String newPassword) {
        if (!passwordEncoder.matches(oldPassword, user.getPassword())) {
            throw FileSharingExceptions.wrongPassword();
        }
        user.setPassword(passwordEncoder.encode(newPassword));
    }

    private Map<KnownAuthority, FileSharingUserAuthority> getAdminAuthorities() {
        return authorityRepository.findAllByIdIn(AuthorityRepository.ADMIN_AUTHORITIES)
                .collect(Collectors.toMap(
                        FileSharingUserAuthority::getId,
                        Function.identity(),
                        (e1, e2) -> e2,
                        () -> new EnumMap<>(KnownAuthority.class)));
    }

    private Map<KnownAuthority, FileSharingUserAuthority> getRegularUserAuthorities() {
        FileSharingUserAuthority authority = authorityRepository
                .findById(KnownAuthority.ROLE_USER)
                .orElseThrow(() -> FileSharingExceptions.authorityNotFound(KnownAuthority.ROLE_USER.name()));
        Map<KnownAuthority, FileSharingUserAuthority> authorities = new EnumMap<>(KnownAuthority.class);
        authorities.put(KnownAuthority.ROLE_USER, authority);
        return authorities;
    }

    private void validateUniqueFields(SaveUserRequest request) {
        String email = request.email();
        if (userRepository.existsByEmail(email)) {
            throw FileSharingExceptions.duplicateEmail(email);
        }
        String nickname = request.nickname();
        if (userRepository.existsByNickname(nickname)) {
            throw FileSharingExceptions.duplicateNickname(nickname);
        }
    }
}
