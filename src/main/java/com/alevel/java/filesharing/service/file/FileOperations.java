package com.alevel.java.filesharing.service.file;

import com.alevel.java.filesharing.model.file.response.FileMetadataResponse;
import com.alevel.java.filesharing.model.file.response.FileResourceResponse;
import com.alevel.java.filesharing.model.file.request.UpdateFileSharingOptionsRequest;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;
import java.util.UUID;

public interface FileOperations extends FileMetadataOperations {

    FileMetadataResponse store(MultipartFile file, Authentication authentication);

    FileMetadataResponse replace(UUID fileId, MultipartFile file, Authentication authentication);

    FileMetadataResponse share(UUID fileId, UpdateFileSharingOptionsRequest request, Authentication authentication);

    Optional<FileResourceResponse> loadAsResource(UUID fileId, Authentication authentication);

    void deleteById(UUID id, Authentication authentication);

}
