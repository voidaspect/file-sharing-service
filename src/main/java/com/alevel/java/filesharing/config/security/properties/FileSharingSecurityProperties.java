package com.alevel.java.filesharing.config.security.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Map;

@Validated
@ConfigurationProperties(prefix = "file-sharing.security")
public class FileSharingSecurityProperties {

    @Valid
    @NestedConfigurationProperty
    private FileSharingJWTProperties jwt;

    private Map<@NotBlank String, @Valid FileSharingAdminProperties> admins;

    public FileSharingJWTProperties getJwt() {
        return jwt;
    }

    public void setJwt(FileSharingJWTProperties jwt) {
        this.jwt = jwt;
    }

    public Map<String, FileSharingAdminProperties> getAdmins() {
        return admins;
    }

    public void setAdmins(Map<String, FileSharingAdminProperties> admins) {
        this.admins = admins;
    }
}
