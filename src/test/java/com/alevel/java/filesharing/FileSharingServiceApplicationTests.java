package com.alevel.java.filesharing;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("h2db")
class FileSharingServiceApplicationTests {

    @Test
    void contextLoads() {
    }

}
